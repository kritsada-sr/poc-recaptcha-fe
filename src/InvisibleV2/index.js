import React, { Component } from 'react'
import axios from 'axios'
import SweetAlert from 'sweetalert-react'
import 'sweetalert/dist/sweetalert.css'

export class InvisibleV2 extends Component {
  state = {
    name: '',
    showPopup: false,
    response: true,
  }

  submitCaptcha = (name, captcha) => {
    axios.post('http://localhost:3001/captcha/v2/invisible', {
      name,
      "g-recaptcha-response": "03ADlfD19tzSgvawC8tUgq_nAzx2emWAKT5_mV0Tvay0xdKDKv3bnqNt5vSHVWdiGmrxsXGbNps-6_3gfpkj8IuwKOrpkFdEYHDn0LJ07XXFryG4wUcr1JS6Go8K1MojHXpQKuu5-E2WTx7lbPktG-QDgOqt_hQ14fa5ErII5IVbQpgP9SEGS_5Bf3GkZFv0sQhD7UrM6hZcliQUkviyV6KncaSXA7Z8tJirSGSseG_SZT6J0_TepnMWXXr-8ENIIuAVCMwGcqzn2rxs14mfj_ybH_iD3xhCnT1g"
    })
      .then(res => this.setState({
        showPopup: true,
        response: res.data.success,
      }))
      .catch((error) => console.error(error))
  }

  render() {
    window.testFunction = (captcha) => {
      const name = this.state.name

      this.submitCaptcha(name, captcha)
    }
    const sampleFERequest = `
    {
      "name": "Poj",
      "g-recaptcha-response": "03ADlfD18htyyikAzvxvsHFjk4nykwzi7RcwaZNo_Ha1hOL1m4BIYdqympcRiJ7s0VyX8LeQnqEycchbzJVqXLsn02VEjWZIzQHKToVecSZ0-oO-mg6TSJVZTb2Myat2Lipq_q0Hp41QBMTt-4q6J9qP4oE37mo4CqtVFAyoHx_BGKR0m0JoMwcI2DiUrzly__jSfoHTaZGChJYCfUzmoQPtOx0cIBbP5UsZSKuLPtgo31TKKZ7u6QZPHyvoj1oceIANN2t2mm_3FO-OY0iYgJrxlugnppaiNgbg"
    }
    `
    const sampleBEGetResponse = `
    {
      "data": "{
        "challenge_ts": "2018-11-22T05:58:03Z",
        "hostname": "localhost"
        "success": true,
      }",
      "message": "success"
    }
    `

    return (
      <div>
        <h3>[V2] - Invisible</h3>
        <div>
          <div className="form-group">
            <label htmlFor="name">Name:</label>
            <input type="text" className="form-control" name="name" onChange={(e) => this.setState({ name: e.target.value })} />
          </div>
          <div className="form-group">
            <button
              className="g-recaptcha btn btn-success"
              data-sitekey="6LfOHnwUAAAAAEZaJiMg4l6B5jqSCFQP1Arvbq_w"
              data-callback="testFunction">
              Submit
            </button>
          </div>
          <div>
            <h2>FE request</h2>
            <pre>
              {sampleFERequest}
            </pre>
          </div>
          <div>
            <h2>BE get response</h2>
            <pre>
              {sampleBEGetResponse}
            </pre>
          </div>
        </div>
        <SweetAlert
          show={this.state.showPopup}
          title="Response"
          text={this.state.response ? 'sucesss' : 'fail'}
          onConfirm={() => this.setState({ showPopup: false })}
        />
      </div>
    )
  }
}

export default InvisibleV2
