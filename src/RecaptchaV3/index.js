import React, { Component } from 'react'
import axios from 'axios'
import SweetAlert from 'sweetalert-react'
import 'sweetalert/dist/sweetalert.css'

const isReady = () => ((typeof window !== 'undefined') && (typeof window.grecaptcha !== 'undefined'))

let readyCheck

export class RecaptchaV3 extends Component {
  state = {
    ready: isReady(),
    name: '',
    captcha: '',
    showPopup: false,
    response: true,
  }
  siteKey = '6LfqJ3wUAAAAAEjNBcTlKyHI2S2N3VpcNB71B_nO'

  componentWillMount = () => {
    this.loadReCaptcha(this.siteKey)

    if (!this.state.ready) {
      readyCheck = setInterval(this._updateReadyState, 1000);
    }
  }

  componentDidMount = () => {
    if (!!(this.state.ready)) {
      this.executeRecaptcha(this.siteKey)
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.ready && !prevState.ready) {
      this.executeRecaptcha(this.siteKey)
    }
  }

  _updateReadyState = () => {
    if (isReady()) {
      window.grecaptcha.ready(() => {
        this.setState({ ready: true });
      })
      clearInterval(readyCheck);
    }
  }

  loadReCaptcha = (siteKey) => {
    const script = document.createElement("script")
    script.src = `https://www.google.com/recaptcha/api.js?render=${siteKey}`
    document.body.appendChild(script)
  }

  executeRecaptcha = (siteKey) => {
    const action = 'login'
    window.grecaptcha.execute(siteKey, { action })
      .then((token) => {
        this.setState({ captcha: token })
      })
  }

  submitCaptcha = () => {
    const { name, captcha } = this.state

    axios.post('http://localhost:3001/captcha/v3', {
      name,
      "g-recaptcha-response": captcha
    })
      .then(res => this.setState({
        showPopup: true,
        response: res.data.success,
      }))
      .catch((error) => console.error(error))
  }

  render() {
    const sampleFERequest = `
    {
      "name": "Poj",
      "g-recaptcha-response": "03ADlfD19v-zcCGCnijJutz7CptvtleWAX4ODAgUKeTxrW7Tlu6KrYotYDCOvNLfP4A51zHn0CcHcyrs5JZvhHXwUlFwV5-oE5kl_zfAw5pf459ML2SFe6elDCUFTw5j4PeTTjerDxpVe3TKb1fY1NNmyASMvcleDFLrllVHECDiYPbKxHOMQAPRllt-Kqn45KVlPpKIAKgr1OAj8VJMwyhvxM4FP0f95X7EphGTjYB7GJMRfWnM4x4Jyw8RBcB2H3ZajiA3YWGgHLYHpkD5WM3VASxFZ69tFQGw"
    }
    `
    const sampleBEGetResponse = `
    {
      "data": "{
        "action": "login",
        "challenge_ts": "2018-11-22T05:58:03Z",
        "hostname": "localhost",
        "score": 0.9,
        "success": true
      }",
      "message": "success"
    }
    `

    return (
      <div>
        <h3>[V3] - Recaptcha</h3>
        <div>
          <div className="form-group">
            <label htmlFor="name">Name:</label>
            <input type="text" className="form-control" name="name" onChange={(e) => this.setState({ name: e.target.value })} />
          </div>
          <div className="form-group">
            <button className="btn btn-success" onClick={this.submitCaptcha}>
              Submit
            </button>
          </div>
          <div>
            <h2>FE request</h2>
            <pre>
              {sampleFERequest}
            </pre>
          </div>
          <div>
            <h2>BE get response</h2>
            <pre>
              {sampleBEGetResponse}
            </pre>
          </div>
        </div>
        <SweetAlert
          show={this.state.showPopup}
          title="Response"
          text={this.state.response ? 'sucesss' : 'fail'}
          onConfirm={() => this.setState({ showPopup: false })}
        />
      </div>
    )
  }
}

export default RecaptchaV3
