import React, { Component } from 'react'
import CheckBoxV2 from './CheckBoxV2'
import InvisibleV2 from './InvisibleV2'
import RecaptchaV3 from './RecaptchaV3'

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Google Recaptcha Example</h1>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-4">
              <CheckBoxV2 />
            </div>
            <div className="col-md-4">
              <InvisibleV2 />
            </div>
            <div className="col-md-4">
              <RecaptchaV3 />
            </div>
          </div>
        </div>

      </div>
    )
  }
}

export default App;
