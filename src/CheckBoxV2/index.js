import React, { Component } from 'react'
import axios from 'axios'
import SweetAlert from 'sweetalert-react'
import 'sweetalert/dist/sweetalert.css'

export class CheckBoxV2 extends Component {
  state = {
    showPopup: false,
    response: true,
  }

  submitCaptcha = (evt) => {
    evt.preventDefault()
    const name = evt.target.name.value
    const captcha = evt.target['g-recaptcha-response'].value

    axios.post('http://localhost:3001/captcha/v2/checkbox', {
      name,
      "g-recaptcha-response": captcha
    })
      .then(res => this.setState({
        showPopup: true,
        response: res.data.success,
      }))
      .catch((error) => console.error(error))
  }

  render() {
    const sampleFERequest = `
    {
      "name": "Poj",
      "g-recaptcha-response": "03ADlfD19qJFDwsnzr5WH93MBVhfN73hOKDhjcuskinUWQvtMIj31tBSc6HLA5w7XP4Dc8BxJyi7jphEuiGJxjVTj4wheWzmthz0u381kdc7H5aybY-WOMB2x71B4v3F6-MwG8Luyuc12wDFBN79Vwc1BeEiMj6XjXEA1IKEkN0_u0HD59nuSc4obgVig2olVrW_4kXLLZGIia3pZG-MjZjuddZzty9JdFW0ijkhKVJOO8AaKSRHe_A2Q3jSpQFYkd4AzD1bA8JZ7QSk-egyyewmIIbf-e12CmSUOYAIpawGxO-4Qx3YmGPlY"
    }
    `
    const sampleBEGetResponse = `
    {
      "data": "{
        "challenge_ts": "2018-11-22T06:02:54Z",
        "hostname": "localhost"
        "success": true,
      }",
      "message": "success"
    }
    `

    return (
      <div>
        <h3>[V2] - Checkbox</h3>
        <form onSubmit={this.submitCaptcha}>
          <div className="form-group">
            <label htmlFor="name">Name:</label>
            <input type="text" className="form-control" name="name" />
          </div>
          <div className="form-group">
            <div
              className="g-recaptcha"
              data-sitekey="6Ld88XsUAAAAALGdx9ZucFjXFXEf9roj_1DnPgh4"
            />
          </div>
          <div className="form-group">
            <button className="btn btn-success">Send</button>
          </div>
          <div>
            <h2>FE request</h2>
            <pre>
              {sampleFERequest}
            </pre>
          </div>
          <div>
            <h2>BE get response</h2>
            <pre>
              {sampleBEGetResponse}
            </pre>
          </div>
        </form>
        <SweetAlert
          show={this.state.showPopup}
          title="Response"
          text={this.state.response ? 'sucesss' : 'fail'}
          onConfirm={() => this.setState({ showPopup: false })}
        />
      </div>
    )
  }
}

export default CheckBoxV2
